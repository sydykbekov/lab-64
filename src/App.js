import React, {Component, Fragment} from 'react';
import './App.css';
import HomePage from "./containers/Header/Header";
import {Route, Switch} from "react-router-dom";
import AddBlog from "./containers/AddBlog/AddBlog";
import About from "./containers/About/About";
import Contacts from "./containers/Contacts/Contacts";
import AddPostsToDOM from "./containers/AddPostsToDOM/AddPostsToDOM";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Route path="/" component={HomePage} />
                <Switch>
                    <Route path="/" component={AddPostsToDOM} />
                    <Route path="/posts" component={AddPostsToDOM} />
                    <Route path="/add" component={AddBlog}/>
                    <Route path="/about" component={About}/>
                    <Route path="/contacts" component={Contacts}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
