import React, {Component, Fragment} from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";

class Header extends Component {
    render() {
        return (
            <Fragment>
                <Navbar inverse collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="/">My Blog</a>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav pullRight>
                            <NavItem eventKey={1} href="/posts">
                                Home
                            </NavItem>
                            <NavItem eventKey={2} href="/add">
                                Add
                            </NavItem>
                            <NavItem eventKey={3} href="/about">
                                About
                            </NavItem>
                            <NavItem eventKey={4} href="/contacts">
                                Contacts
                            </NavItem>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </Fragment>

        )
    }
}

export default Header;