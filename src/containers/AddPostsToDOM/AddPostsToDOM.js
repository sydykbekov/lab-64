import React, {Component} from 'react';
import './AddPostsToDOM.css';
import {Button, Jumbotron} from "react-bootstrap";
import axios from 'axios';

class AddPostsToDOM extends Component {
    state = {
        posts: [{title: 'title', date: '2.02.2018'}]
    };
    getPosts = () => {
        axios.get('/posts.json').then(response => {
            console.log(response.data);
        });
    };
    componentDidMount() {
        this.getPosts();
    }
    render() {
        return (
            this.state.posts.map((post, key) => {
                return (
                    <Jumbotron key={key} className="jumbotron">
                        <span>{post.date}</span>
                        <h3>{post.title}</h3>
                        <p>
                            <Button bsStyle="primary">Read more >></Button>
                        </p>
                    </Jumbotron>
                )
            })
        )
    }
}

export default AddPostsToDOM;