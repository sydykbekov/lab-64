import React, {Component} from 'react';

class AddBlog extends Component {
    render() {
        return (
            <div className="form">
                <h3>Add new post</h3>
                <label>Title <br/>
                    <input type="text"/>
                </label>
                <label>Description <br/>
                    <textarea cols="30" rows="10" />
                </label>
                <button>Save</button>
            </div>
        )
    }
}

export default AddBlog;


